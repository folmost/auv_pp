#!/usr/bin/env python
import sys, getopt, rospy, numpy as np
from planning import RRTstar, GA, INCREMENT_DISTANCE
from script import OctomapNode,remove_start_offset
from std_msgs.msg import String, Time
from time import time, sleep
import pyautogui
#from geometry_msgs.msg import PointStamped
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import LaserScan
from planning import bezier, over_sampling, filter_path
from evaluation import Evaluation
import pandas as pd

algorithm = {
    'rrt*': RRTstar,
    'rrt_star': RRTstar,
    'ga': GA,
    #'a*': main_a_star,
    #'a_star': main_a_star,
}

class PathPlanning:
    def __init__(self):
        self.node = OctomapNode(node_name='path_planning')
        self.node.setup()
        self.planning_algorithm = None
        self.save_stats = False
        self.display = True
        self.goal_point = None
        self.scan = False
        self.nodes = []
        self.id = 0
        self.optimize_path = True
        #rospy.Subscriber('/clicked_point', PointStamped, self.callback) #Para 3D.
        rospy.Subscriber('/move_base_simple/goal', PoseStamped, self.call_goal_point)
        rospy.Subscriber('/eca_a9/sss_right', LaserScan, self.call_scan)

    def call_goal_point(self, msg):
        #goal_point = [msg.point.x, msg.point.y, msg.point.z]
        self.goal_point = [-960, 1610, -10]#[msg.pose.position.x, msg.pose.position.y, -10]
        #self.goal_point = [-920, 2080, -8]
        #self.goal_point = [-980, 1150, -5]
        self.start_planning()

    def call_scan(self, msg):
        self.range_min = min(msg.ranges[0:40])
        if self.range_min < 5 and not self.scan:
            #self.node.get_circle()
            self.scan = True
            #self.start_planning()

    def start_planning(self):
        i = 1
        path_length = []
        std = []
        max_angle = []
        average_angle = []
        planning_number = []
        algo_name = []
        processing_time = []
        cpu = []
        ram = []
        while i <= 1:
            rospy.loginfo("Goal point: " + str(self.goal_point) + " Iteration: " + str(i))
            actual_position = [5,-5,-5]#self.node.get_position()
            algoritmo = rospy.get_param('~algoritmo', 'rrt_star')
            self.planning_algorithm = algorithm[algoritmo](self.node, actual_position, self.goal_point, world_dim=[-1050, 1000, -550, 2350, -18, 5])
            start_time = time()
            if not self.save_stats:
                self.nodes = []
                self.id = 0
            path, self.nodes, self.id, cpu_, ram_ = self.planning_algorithm.planning(self.nodes, self.id, display=self.display)
            #self.node.viz_path(path, color=(0.0,1.0,0.0), scale=15)
            if self.optimize_path:
                path = filter_path(path, self.node)
                #path = self.good_path()
                path = over_sampling(path, max_length=INCREMENT_DISTANCE)
                #self.node.viz_path(path, color=(1.0,1.0,0.0), scale=15)
            elapsed_time = time() - start_time
            ev = Evaluation(path, elapsed_time, i, algoritmo)
            path_length.append(ev.path_length(path))
            angles, average = ev.path_rotation(path)
            std.append(np.std(angles))
            max_angle.append(max(angles))
            average_angle.append(average)
            planning_number.append(i)
            algo_name.append(algoritmo)
            processing_time.append(elapsed_time)
            cpu.append(cpu_)
            ram.append(ram_)


            # Publica la ruta en simulador
            self.node.publish_path(path)
            rospy.loginfo("Tiempo de ejecucion: " + str(elapsed_time))
            sleep(1)
            self.scan = False
            i += 1

        data = {
                'World':'mangalia.world',
                'Planning number':planning_number,
                'Algorithm':algo_name,
                'Path length':path_length,
                'Processing time':processing_time,
                'Std of angles':std,
                'Max angle':max_angle,
                'Average angle':average_angle,
                'CPU':cpu,
                'RAM':ram,
            }

        df = pd.DataFrame(data, columns=['World', 'Planning number', 'Algorithm', 'Path length', 'Processing time', 'Std of angles', 'Max angle', 'Average angle', 'CPU', 'RAM'])
        df.to_csv('generation-50.csv', sep=';')

    def bad_path(self):
        path = [(5, -5, -5), (47, -19, 0), (107, -142, 0), (-260, -90, 0), (-551, -357, 0), (-564, -512, 0), (-711, -511, 0), (-918, -505, 0), (-1030, -433, 0), (-848, -220, 0), (-1020, -26, 0), (-855, 107, 0), (-920, 492, 0), (-917, 810, 0), (-992, 702, 0), (-877, 424, 0), (-589, 513, 0), (-505, 154, 0), (-244, 264, -5), (-65, -60, -5), (103, -375, -5), (416, -165, -5), (685, -392, -4), (833, -140, -1), (678, -16, 0), [659.6, -7.9, 0.0], [641.3, 0.13, 0.0], [623.0, 8.19, 0.0], [604.7, 16.2, 0.0], [586.4, 24.7, 0.0], (231, 181, 0), (578, 350, 0), (757, 624, 0), (739, 757, 0), (688, 758, 0), (562, 1040, -3), [551.7, 1056.3, -3.167], [540.4, 1073.7, -3.334], [530.1, 1090.2, -3.6501], [519.07, 1107.93, -3.8668], [508.336, 1124.67, -3.0835], [498.6, 1141.4, -3.3002], [487.86, 1158.13, -3.5169], [476.123, 1175.87, -3.7336], [466.386, 1192.6, -3.9503], [455.65, 1209.34, -3.167], [444.91, 1226.07, -3.3837], [434.174, 1243.8, -3.6004], [423.436, 1260.54, -4.817], [412.7, 1276.27, -4.0337], [402.96, 1293., -4.25035], [391.224, 1310.74, -4.467], [380.487, 1327.47, -4.68365], (178, 1650, -6), [165.784, 1665.16, -6.47374], [152.57, 1680.32, -6.9475], [139.353, 1695.49, -6.4212], [126.137, 1711.65, -6.895], [113.922, 1726.8, -6.3687], [100.706, 1741.97, -6.8425], [87.49, 1756.13, -6.3162], [74.275, 1772.3, -6.79], [61.0584, 1787.46, -6.2637], [48.842, 1802.62, -6.73745], [35.6256, 1817.78, -6.2112], [22.4093, 1833.94, -6.6849], [10.193, 1848.1, -6.1587], [-2.0231, 1863.27, -6.6324], [-15.2393, 1879.43, -6.1062], (-262, 2170, -8), [-261.63, 2150.47, -8.4935], [-260.255, 2130.94, -8.987], [-260.88, 2110.4, -8.4804], [-259.51, 2090.9, -8.9739], [-259.14, 2070.36, -8.4673], [-258.765, 2050.83, -8.608], [-258.9, 2030.3, -8.4543], [-257.02, 2010.78, -8.9477], [-256.65, 1990.25, -8.4412], [-256.275, 1970.72, -8.9347], [-255.9, 1950.2, -8.4281], [-255.524, 1930.67, -8.9216], [-254.15, 1910.14, -8.415], [-254.774, 1890.61, -8.9085], [-253.398, 1870.08, -8.402], (-243, 1490, -9), [-257.705, 1475.06, -9.0], (-536, 1200, -9), [-544.98, 1218.96, -9.7914], [-552.97, 1236.92, -9.5828], [-560.95, 1254.88, -9.3742], [-568.94, 1272.84, -9.1656], [-576.92, 1291.8, -9.957], [-585.91, 1309.76, -9.7484], (-747, 1670, -10), (-960, 1610, -10)] 

        return path

    def good_path(self):
        path = [(5, -5, -5), (63, -21, -5), (82.99, -21.12, -5), (125, -21.4, -5), (173.45, -9.056, -5.68), (196, -3.31, -6), (200.45, 5.64, -6.15), (225, 55, -7), (76.15, 268.17, -7.44), (-24.03, 411.65, -7.74), (-111, 536.2, -8),(-202.6, 667.38, -8),  (-308.52, 819.06, -8), (-423, 983, -8), (-543.26, 1155.16, -8.63), (-613, 1255, -9), (-763, 1470, -9), (-960, 1610, -10)] 
        return path


if __name__ == '__main__':
    print('Starting Path Planning')
    try:
        node = PathPlanning()
        rospy.spin()
        sys.exit(1)
    except rospy.ROSInterruptException:
        print('Caught Exception')
