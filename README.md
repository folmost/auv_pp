## Instalación

Instalar UUV_simulator para ROS Melodic

- `melodic` (See [installation instructions for ROS Melodic](https://wiki.ros.org/melodic/Installation/Ubuntu))

```bash
sudo apt install ros-melodic-uuv-simulator
```

Luego, para simular ECA_A9 AUV, se debe clonar el paquete en la carpeta 'src' de espacio de trabajo catkin
```
cd ~/catkin_ws/src
git clone https://github.com/uuvsimulator/eca_a9.git
```

Clonar paquete para planificación de ruta en carpeta 'src' de espacio de trabajo catkin.
```
cd ~/catkin_ws/src
git clone https://fabian_olmos@bitbucket.org/folmost/auv_pp.git
```

Construir espacio de trabajo catkin.
```bash
cd ~/catkin_ws
catkin_make # o <catkin build>, si usted utiliza catkin_tools
```
---
## Configurar Octomap
Primero instalar octomap
```bash
sudo apt install ros-melodic-octomap
```

Luego, instalar octomap_server, que es el nodo que se usará en ROS
```bash
sudo apt install ros-melodic-octomap-mapping
```

Por último, para poder usar los datos de octomap se debe inslatar la librería de octomap en python
```bash
sudo apt install liboctomap-dev
sudo apt install libdynamicedt3d-dev
git clone --recursive https://github.com/wkentaro/octomap-python.git
cd octomap-python
python2 setup.py build
python2 setup.py install
```
---

## Paquetes python
Numba
```bash
sudo apt install python-numba
```
Pyautogui
```bash
pip install pyautogui
```

Panda
```bash
pip install panda
```

## Ejemplo de uso

Primero debemos iniciar el AUV ECA_A9 en un mundo de Gazebo, además el lanzador inicia un controlador para los movimientos del AUV.
```bash
roslaunch auv_pp start_auv.launch 
```
Luego, iniciamos un planificador, que por ahora solo envía al AUV a un punto determinado.
```bash
roslaunch auv_pp start_waypoint_list.launch algoritmo:=rrt*
```

Cambiar en algoritmo por ga para utilizar algoritmos genéticos.
