import numpy as np, math, rospy

class Evaluation:
    def __init__(self, path, processing_time, planning_number='1', algo_name='rrt*'):
        rospy.loginfo('Computing path stats...')

        rospy.loginfo('Writing path stats...')
        #f = open('evaluation_paths.txt', 'a') 
        #f.writelines("%s\n" % s for s in prev_data)
        #f.close()

    def path_length(self, path):
        if len(path) < 2:
            return 0.
        path_length = 0.
        waypoints_distance = []
        for i in range(len(path) - 1):
            path_length += self.dist(path[i], path[i + 1])
        return path_length


    def path_rotation(self, path):
        # Compute the angle of rotation along a path
        angles = []
        average_angle = 0
        for i in range(1, len(path)-1):
            a, b, c = np.array(path[i-1]), np.array(path[i]), np.array(path[i+1])
            vec1, vec2 = b - a, c - b
            angles.append(self.angle_between(vec1, vec2))
            average_angle += self.angle_between(vec1, vec2)
        std = np.std(angles)
        max_angle = max(angles)
        return angles, (average_angle/len(angles))


    def angle_between(self, v1, v2):
        # Returns the angle between vectors 'v1' and 'v2'
        v1_u = self.unit_vector(v1)
        v2_u = self.unit_vector(v2)
        return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)) * 180 / np.pi


    def unit_vector(self, vector):
        # Returns the unit vector of the vector
        return vector / np.linalg.norm(vector) if np.linalg.norm(vector) != 0 else vector

    def dist(self, p1, p2):
        return math.sqrt((p2[0] - p1[0])**2 + (p2[1]-p1[1])**2 + (p2[2]-p1[2])**2)
    
