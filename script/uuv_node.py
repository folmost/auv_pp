#!/usr/bin/env python
import rospy, numpy as np
import sys
from uuv_control_msgs.msg import Waypoint, WaypointSet
from uuv_control_msgs.srv import InitWaypointSet
from std_msgs.msg import String, Time
from numpy import pi
from nav_msgs.msg import Odometry
from tf_quaternion.transformations import euler_from_quaternion

class UUVNode(object):
    def __init__(self):
        self.inertial_frame_id = 'world_ned'
        self.max_forward_speed = 2.0
        self.pos = 0

    def setup(self):
        rospy.init_node(self.node_name, anonymous=True)
        rospy.Subscriber('pos', Odometry, self.call_position)

    def add_waypoint(self, x, y ,z):
        wp = Waypoint()
        wp.header.stamp = rospy.Time().now()
        wp.header.frame_id = self.inertial_frame_id
        wp.point.x = x
        wp.point.y = y
        wp.point.z = z
        wp.max_forward_speed = self.max_forward_speed
        wp.heading_offset = 0.0
        return wp

    def start_waypoint(self, num_wps, x, y, z):
        if rospy.is_shutdown():
            print('ROS master not running!')
            sys.exit(-1)
        wps = list()
        for i in range(num_wps):
            wp_msg = self.add_waypoint(x[i], y[i], z[i])
            wps.append(wp_msg)

        start_time = rospy.Time.now().to_sec()
        start_now = True

        if rospy.has_param('~start_time'):
            start_time = rospy.get_param('~start_time')
            if start_time < 0.0:
                rospy.logerr('Negative start time, setting it to 0.0')
                start_time = 0.0
                start_now = True
            else:
                start_now = False
        else:
            start_now = True
        try:
            rospy.wait_for_service('start_waypoint_list', timeout=20)
        except rospy.ROSException:
            print('Service not available! Closing node...')
            sys.exit(-1)
        try:
            traj_gen = rospy.ServiceProxy('start_waypoint_list', InitWaypointSet)
        except rospy.ServiceException as e:
            print('Service call failed, error={}'.format(e))
            sys.exit(-1)
        rospy.loginfo('Start time=%.2f s' % start_time)

        rospy.loginfo('Generating trajectory!!!')
        success = traj_gen(Time(rospy.Time.from_sec(start_time)),
                        start_now,
                        wps,
                        self.max_forward_speed,
                        self.heading_offset,
                        self.interpolator)
        if success:
            rospy.loginfo('Trajectory successfully generated!')
        else:
            rospy.loginfo('Failed')

    def publish_path(self, path):
        rospy.loginfo('Starting the waypoint list')
        param_labels = ['max_forward_speed', 'heading_offset', 'interpolator']
        params = dict()
        for label in param_labels:
            if not rospy.has_param('~' + label):
                print('{} must be provided for the trajectory generation!'.format(label))
                sys.exit(-1)

            params[label] = rospy.get_param('~' + label)

        if params['max_forward_speed'] <= 0:
            print('Velocity limit must be positive')
            sys.exit(-1)
        self.max_forward_speed = params['max_forward_speed']
        self.interpolator = String(rospy.get_param('~interpolator', 'dubins'))
        self.heading_offset = params['heading_offset']
        x = []
        y = []
        z = []
        for i in range(len(path)):
            _y, _x, _z = path[i]
            x.append(_x)
            y.append(_y)
            z.append(abs(_z))
        num_wps = len(path)
        # If no start time is provided: start *now*.
        self.start_waypoint(num_wps, x, y, z)


    def get_position(self):
        position = [self.pos[0], self.pos[1], self.pos[2]]
        return position

    def call_position(self, msg):
        self.pos = [msg.pose.pose.position.x,
            msg.pose.pose.position.y,
            msg.pose.pose.position.z]

        self.quat = [msg.pose.pose.orientation.x,
                msg.pose.pose.orientation.y,
                msg.pose.pose.orientation.z,
                msg.pose.pose.orientation.w]


    def get_circle(self):
        print("********Start Circle*********")
        num_points = 30
        radius = 20
        #step_theta = 2 * np.pi / num_points
        #for i in range(num_points):
        #    angle = i * step_theta + theta_offset
        #    self.x.(np.cos(angle) * radius + center.x)
        #    y = np.sin(angle) * radius + center.y)
        #    self.z.append(center.z)
        rot = euler_from_quaternion(self.quat) #quat to euler
        #matriz de rotacion en eje z
        frame = np.array([
            [np.cos(rot[2]), -np.sin(rot[2]), 0],
            [np.sin(rot[2]), np.cos(rot[2]), 0],
            [0, 0, 1]])
        print("Frame ",frame,frame[:, 0])
        self._idle_circle_center = (self.pos + 0.8 * self.max_forward_speed * frame[:, 0].flatten()) + radius * frame[:, 1].flatten()
        print("Circle center ",self._idle_circle_center, "Pos: ",self.pos)
        self._idle_z = self.pos[2]

        phi = lambda u: 2 * np.pi * u + rot[2] - np.pi / 2
        u = lambda angle: (angle - rot[2] + np.pi / 2) / (2 * np.pi)

        vec = self.pos - self._idle_circle_center
        vec /= np.linalg.norm(vec)
        u_init = u(np.arctan2(vec[1], vec[0]))
        x = []
        y = []
        z = []
        for i in np.linspace(u_init, u_init + 1, num_points):
            x.append(self._idle_circle_center[0] + radius * np.cos(phi(i)))
            y.append(self._idle_circle_center[1] + radius * np.sin(phi(i)))
            z.append(abs(self._idle_z))
        self.max_forward_speed=0.2 * self.max_forward_speed
        self.start_waypoint(num_points, x, y, z)

