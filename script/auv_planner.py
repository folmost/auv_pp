#!/usr/bin/env python
# Copyright (c) 2016 The UUV Simulator Authors.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from __future__ import print_function
import rospy
import sys
from uuv_control_msgs.srv import GoTo
from uuv_control_msgs.msg import Waypoint
from numpy import pi
from std_msgs.msg import Time


if __name__ == '__main__':
    print('Starting the Go to')
    rospy.init_node('go_to')

    if rospy.is_shutdown():
        print('ROS master not running!')
        sys.exit(-1)

    wp = Waypoint()
    wp.header.stamp = rospy.Time().now()
    wp.header.frame_id = 'world_ned'
    wp.point.x = -30
    wp.point.y = -100
    wp.point.z = 15
    wp.max_forward_speed = 2.0
    wp.use_fixed_heading = False
    wp.heading_offset = 0.0

    param_labels = ['max_forward_speed', 'interpolator']
    params = dict()

    for label in param_labels:
        if not rospy.has_param('~' + label):
            print('{} must be provided for the trajectory generation!'.format(label))
            sys.exit(-1)

        params[label] = rospy.get_param('~' + label)

    if params['max_forward_speed'] <= 0:
        print('Velocity limit must be positive')
        sys.exit(-1)

    try:
        rospy.wait_for_service('go_to', timeout=20)
    except rospy.ROSException:
        print('Service not available! Closing node...')
        sys.exit(-1)

    try:
        traj_gen = rospy.ServiceProxy('go_to', GoTo)
    except rospy.ServiceException as e:
        print('Service call failed, error={}'.format(e))
        sys.exit(-1)

    print('Generating trajectory!!!')

    success = traj_gen(wp,
                       params['max_forward_speed'],
                       params['interpolator'])

    if success:
        print('Trajectory successfully generated!')
    else:
        print('Failed')
