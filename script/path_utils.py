from math import pi, sin, cos

"""
fix_path_orientation()
Fix wrong TF when using mission and offboard system
The reference axis are rotated clock wise (-Z)
"""
def fix_path_orientation(path):
    new_path = []
    for x, y, z in path:
        x, y, z = y, -x, z
        new_path.append([x, y, z])
    return new_path

"""
remove_start_offset()
Remove the offset from the starting point in the path
as if the path was starting from the (0, 0) point.
"""
def remove_start_offset(path):
    x_start, y_start, z_start = path[0]
    new_path = [[0, 0, z_start]]

    for x, y, z in path:
        new_path.append([x - x_start, y - y_start, z])
    print("New path: ",new_path)
    return new_path 
    
