from visualization_msgs.msg import MarkerArray, Marker
import rospy
from uuv_node import UUVNode

class Visualization(UUVNode):
    def setup(self):
        super(Visualization, self).setup()
        self._visualization_marker_pub = rospy.Publisher(
                '/visualization_marker', Marker, queue_size=100)
        self._visualization_near_pub = rospy.Publisher(
                '/near_point', Marker, queue_size=100)
        self._visualization_rand_pub = rospy.Publisher(
                '/rand_point', Marker, queue_size=100)
        self._visualization_new_pub = rospy.Publisher(
                '/new_point', Marker, queue_size=100)
        self._visualization_path = rospy.Publisher(
                '/path', MarkerArray, queue_size=100)

    def viz_point(self, point=None, color=(1.0,0.0,0.0), id=0, scale=3.0):
        x = float(point[0])
        y = float(point[1])
        z = float(point[2])
        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "world"
        marker.ns = 'nodes'
        marker.id = id
        marker.type = Marker.SPHERE
        marker.action = Marker.ADD
        marker.pose.position.x = x
        marker.pose.position.y = y
        marker.pose.position.z = z
        marker.scale.x = scale
        marker.scale.y = scale
        marker.scale.z = scale

        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]

        self._visualization_marker_pub.publish(marker)

    def viz_point_near(self, point=None, color=(1.0,0.0,0.0), id=0, scale=3.0):
        x = float(point[0])
        y = float(point[1])
        z = float(point[2])

        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "world"
        marker.ns = 'near'
        marker.id = id
        marker.type = Marker.SPHERE
        marker.action = Marker.MODIFY
        marker.pose.position.x = x
        marker.pose.position.y = y
        marker.pose.position.z = z
        marker.scale.x = scale
        marker.scale.y = scale
        marker.scale.z = scale

        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]

        self._visualization_near_pub.publish(marker)

    def viz_point_rand(self, point=None, color=(1.0,0.0,0.0), id=0, scale=3.0):
        x = float(point[0])
        y = float(point[1])
        z = float(point[2])

        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "world"
        marker.ns = 'rand'
        marker.id = id
        marker.type = Marker.SPHERE
        marker.action = Marker.MODIFY
        marker.pose.position.x = x
        marker.pose.position.y = y
        marker.pose.position.z = z
        marker.scale.x = scale
        marker.scale.y = scale
        marker.scale.z = scale

        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]

        self._visualization_rand_pub.publish(marker)

    def viz_point_new(self, point=None, color=(1.0,0.0,0.0), id=0, scale=3.0):
        x = float(point[0])
        y = float(point[1])
        z = float(point[2])

        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "world"
        marker.ns = 'new'
        marker.id = id
        marker.type = Marker.SPHERE
        marker.action = Marker.MODIFY
        marker.pose.position.x = x
        marker.pose.position.y = y
        marker.pose.position.z = z
        marker.scale.x = scale
        marker.scale.y = scale
        marker.scale.z = scale

        marker.color.a = 1.0
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]

        self._visualization_new_pub.publish(marker)

    def viz_path(self, path, color=(1.0,0.0,0.0), id=0, scale=3.0):
        list_waypoints = MarkerArray()
        t = rospy.Time.now()
        if len(path) <= 0:
            marker = Marker()
            marker.header.stamp = t
            marker.header.frame_id = "world"
            marker.ns = 'path'
            marker.id = id
            marker.type = Marker.SPHERE
            marker.action = Marker.ADD
            list_waypoints.markers.append(marker)
        else:
            for i in range(len(path)):
                x = path[i][0]
                y = path[i][1]
                z = path[i][2]
                marker = Marker()
                marker.header.stamp = t
                marker.header.frame_id = "world"
                marker.ns = 'path'
                marker.id = id + i
                marker.type = Marker.SPHERE
                marker.action = Marker.ADD
                marker.pose.position.x = x
                marker.pose.position.y = y
                marker.pose.position.z = z
                marker.scale.x = scale
                marker.scale.y = scale
                marker.scale.z = scale
                marker.color.a = 1.0
                marker.color.r = color[0]
                marker.color.g = color[1]
                marker.color.b = color[2]
                list_waypoints.markers.append(marker)
        self._visualization_path.publish(list_waypoints)

