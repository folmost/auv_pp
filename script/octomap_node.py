#!/usr/bin/env python
import rospy, math, octomap, numpy as np
from octomap_msgs.msg import Octomap
from visualization import Visualization

class OctomapNode(Visualization):
    def __init__(self, node_name):
        super(OctomapNode,self).__init__()
        self.node_name = node_name
        self.octree = octomap.OcTree(0.1)
        self.collision_check = 0

    def setup(self):
        super(OctomapNode, self).setup()
        self.octomap_sub = rospy.Subscriber('/octomap_binary', Octomap, self.octomap_cb)
        self.rate = rospy.Rate(20)
        self.rate.sleep()


    def octomap_cb(self, data):
        rospy.loginfo('Octomap updated !')
        self.octomap = data

        # Read the octomap binary data and load it in the octomap wrapper class
        data = np.array(self.octomap.data, dtype=np.int8).tostring()
        s = '# Octomap OcTree binary file\nid {}\n'.format(self.octomap.id)
        s += 'size 690089\nres {}\ndata\n'.format(self.octomap.resolution)
        s += data

        # An error is triggered because a wrong tree size has been specified in the
        # header. We did not find a way to extract the tree size from the octomap msg
        tree = octomap.OcTree(self.octomap.resolution)
        tree.readBinary(s)
        self.octree = tree


    def cast_ray(self, origin, dest, radius=0., max_dist=-1, display=False):
        self.collision_check += 1
        origin = np.array(origin, dtype=np.double)
        dest = np.array(dest, dtype=np.double)
        direction = dest - origin + 1e-6
        distance = np.linalg.norm(direction)
        direction /= distance
        distance = distance if max_dist == -1 or distance < max_dist else max_dist
        end = dest if max_dist == -1 else origin + direction * max_dist
        #print("Origin: ", origin,"Direction: ", direction,"Destino: ", dest)
        hit = self.octree.castRay(origin, direction, end, ignoreUnknownCells=True, maxRange=distance)
        #print("Hit: ",hit)
        if hit or radius == 0.:
            return hit, end
        # To check the cylinder volume, we cast 4 additional rays
        # axis1 and 2 are in the plane perpendicular to the direction
        if direction[0] == 0 and direction[1] == 0:
            if direction[2] == 0:
                return False, origin
            axis1 = np.array([0., 1., 0.], dtype=np.double)
        else:
            axis1 = np.array([-direction[1], direction[0], 0.], dtype=np.double)           
        axis1 /= np.linalg.norm(axis1)
        axis2 = np.cross(direction, axis1)
        axis2 /= np.linalg.norm(axis2)
        origin1 = origin + axis1 * radius
        origin2 = origin - axis1 * radius
        origin3 = origin + axis2 * radius
        origin4 = origin - axis2 * radius
        for o in [origin1, origin2, origin3, origin4]:
            h = self.octree.castRay(o, direction, end, ignoreUnknownCells=True, maxRange=distance)
            if h:
                return h, end
        #print("Retorno 2: ",h)
        return hit, end
