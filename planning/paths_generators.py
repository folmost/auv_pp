#!/usr/bin/env python
from numba import jit
from random import randint, shuffle
from time import sleep
import random

MAX_ITERATIONS = 100000
RADIUS  = 150
debugging = False
#debugging = True

class PathsGenerators(object):
    def __init__(self, world_dim, ros_node, goal):
        self.world_dim = world_dim
        self.ros_node = ros_node
        self.goal = goal

    @jit
    def get_solutions(self, start):
        #random.seed(10)
        prev_x = start[0]
        prev_y = start[1]
        prev_z = start[2]
        tmp = []
        tmp.append(tuple(start))
        i = 0
        cell = False
        cast_error = False
        actual_point = [prev_x, prev_y, prev_z]
        while i < MAX_ITERATIONS:
            #print("Inicia en: ", actual_point)
            i = i + 1
            ret, tmp = self.path_to_goal(20, actual_point, tmp)
            if ret:
                break
            else:
                xx = randint(0,1)
                yy = randint(0,1)
                zz = randint(0,1)
                ret, tmp = self.random_path(10, actual_point, tmp, xx, yy, zz)
                if ret:
                    break
            actual_point = tmp[-1]
            #sleep(1)
        return tmp

    @jit
    def random_path(self, tries, origin, tmp, xx, yy, zz):
        #print("Inicia busqueda de ruta aleatoria", self.world_dim)
        xmin, xmax, ymin, ymax, zmin, zmax = self.world_dim
        prev_x = origin[0]
        prev_y = origin[1]
        prev_z = origin[2]
        goal_x = False
        goal_y = False
        goal_z = False
        true_cast = 0
        while tries >= 0:
            allowed_point = False
            if xx == 1:
                minx = prev_x
                maxx = prev_x + RADIUS
            else:
                maxx = prev_x
                minx = prev_x - RADIUS
            if yy == 1:
                miny = prev_y
                maxy = prev_y + RADIUS
            else:
                maxy = prev_y
                miny = prev_y - RADIUS
            if zz == 1:
                minz = prev_z
                maxz = prev_z + 1
            else:
                maxz = prev_z
                minz = prev_z - 1
            available_x = range(int(minx), int(maxx+1))
            available_y = range(int(miny), int(maxy+1))
            available_z = range(int(minz), int(maxz+1))
            origin = [prev_x, prev_y, prev_z]
            available_x = self.check_dimension(available_x, xmin, xmax)
            available_y = self.check_dimension(available_y, ymin, ymax)
            available_z = self.check_dimension(available_z, zmin, zmax)
            ret = False
            while not allowed_point:
                next_x = available_x[randint(0,len(available_x)-1)]
                next_y = available_y[randint(0,len(available_y)-1)]
                next_z = available_z[randint(0,len(available_z)-1)]
                dest = [next_x, next_y, next_z]
                #self.ros_node.viz_point_rand(dest, color=(1,1,0), id=1, scale=20)
                #self.ros_node.viz_point_rand(origin, color=(0,1,1), id=2, scale=10)
                ## Revisa si hay obstaculo entre punto origen y destino
                cell = self.ros_node.cast_ray(origin, dest, radius=10)[0]
                new = self.ros_node.cast_ray(origin, dest, radius=10, max_dist=RADIUS*0.4)[1]
                if not (prev_x == next_x and prev_y == next_y and prev_z == next_z):
                    allowed_point = True
                else:
                    tries -= 1
                if tries < 0:
                    break
            # En caso de haber obstaculo retorna el punto de origen.
            if cell:
                true_cast += 1
                next_x = prev_x
                next_y = prev_y
                next_z = prev_z
                if (true_cast > 5) and (len(tmp) > 1):
                    next_x, next_y, next_z = tmp[-2][0], tmp[-2][1], tmp[-2][2]
                    tmp.pop(-1)
                    true_cast = 0
            # Si no hay obstaculo agrega el punto a la ruta y revisa si es que se ha llegado al punto objetivo.
            else:
                true_cast = 0
                tmp.append((next_x, next_y, next_z))
                if self.goal[0] - (RADIUS*0.1) < next_x < self.goal[0] + (RADIUS*0.1):
                    goal_x = True
                if self.goal[1] - (RADIUS*0.1) < next_y < self.goal[1] + (RADIUS*0.1):
                    goal_y = True
                if self.goal[2] - (RADIUS*0.1) < next_z < self.goal[2] + (RADIUS*0.1):
                    goal_z = True
                if goal_x and goal_y and goal_z:
                    ret = True
                    break
                if not self.ros_node.cast_ray(dest, self.goal, radius=10)[0]:
                    goal = [self.goal[0], self.goal[1] + 1, self.goal[2]]
                    test = self.ros_node.cast_ray(dest, goal, radius=15, display=True)[0]
                    if test == False:
                        ret = True
                        tmp.append((self.goal[0], self.goal[1], self.goal[2]))
                    break
            prev_x = next_x
            prev_y = next_y
            prev_z = next_z
            tries -= 1
        return ret, tmp

    def path_to_goal(self, tries, origin, tmp):
        xmin, xmax, ymin, ymax, zmin, zmax = self.world_dim
        prev_x = origin[0]
        prev_y = origin[1]
        prev_z = origin[2]
        goal_tmp = tmp
        goal_x = False
        goal_y = False
        goal_z = False
        ret = False
        true_cast = 0
        while tries >= 0:
            allowed_point = False
            minx = prev_x
            maxx = prev_x
            if self.goal[0] < prev_x:
                minx = prev_x - RADIUS
            if self.goal[0] > prev_x:
                maxx = prev_x + RADIUS
            miny = prev_y
            maxy = prev_y
            if self.goal[1] < prev_y:
                miny = prev_y - RADIUS
            if self.goal[1] > prev_y:
                maxy = prev_y + RADIUS
            minz = prev_z
            maxz = prev_z
            if self.goal[2] < prev_z:
                minz = prev_z - 1
            if self.goal[2] > prev_z:
                maxz = prev_z + 1
            available_x = range(int(minx), int(maxx+1))
            available_y = range(int(miny), int(maxy+1))
            available_z = range(int(minz), int(maxz+1))
            available_x = self.check_dimension(available_x, xmin, xmax)
            available_y = self.check_dimension(available_y, ymin, ymax)
            available_z = self.check_dimension(available_z, zmin, zmax)
            origin = [prev_x, prev_y, prev_z]
            while not allowed_point:
                next_x = available_x[randint(0,len(available_x)-1)]
                next_y = available_y[randint(0,len(available_y)-1)]
                next_z = available_z[randint(0,len(available_z)-1)]
                dest = [next_x, next_y, next_z]
                ## Revisa si hay obstaculo entre punto origen y destino
                #self.ros_node.viz_point_rand(dest, color=(1,1,0), id=1, scale=20)
                #self.ros_node.viz_point_rand(origin, color=(0,1,1), id=2, scale=10)
                cell = self.ros_node.cast_ray(origin, dest, radius=10)[0]
                new = self.ros_node.cast_ray(origin, dest, radius=10, max_dist=RADIUS*0.1)[1]
                if not (prev_x == next_x and prev_y == next_y and prev_z == next_z):
                    allowed_point = True
                else:
                    tries -= 1
                if tries < 0:
                    break
            # En caso de haber obstaculo retorna el punto de origen.
            if cell:
                true_cast += 1
                next_x = prev_x
                next_y = prev_y
                next_z = prev_z
                if (true_cast > 10) and (len(goal_tmp) > 1):
                    next_x, next_y, next_z = goal_tmp[-2][0], goal_tmp[-2][1], goal_tmp[-2][2]
                    goal_tmp.pop(-1)
                    true_cast = 0
            # Si no hay obstaculo agrega el punto a la ruta y revisa si es que se ha llegado al punto objetivo.
            else:
                true_cast = 0
                goal_tmp.append((next_x, next_y, next_z))
                if self.goal[0] - (RADIUS*0.4) < next_x < self.goal[0] + (RADIUS*0.4):
                    goal_x = True
                if self.goal[1] - (RADIUS*0.4) < next_y < self.goal[1] + (RADIUS*0.4):
                    goal_y = True
                if self.goal[2] - (RADIUS*0.4) < next_z < self.goal[2] + (RADIUS*0.4):
                    goal_z = True
                if goal_x and goal_y and goal_z:
                    ret = True
                    tmp = goal_tmp
                    break
                if not self.ros_node.cast_ray(dest, self.goal, radius = 10)[0]:
                    goal = [self.goal[0], self.goal[1] + 1, self.goal[2]]
                    test = self.ros_node.cast_ray(dest, goal, radius=15, display=True)[0]
                    if not test:
                        ret = True
                        goal_tmp.append((self.goal[0], self.goal[1], self.goal[2]))
                        tmp = goal_tmp
                        break
            prev_x = next_x
            prev_y = next_y
            prev_z = next_z
            tries -= 1
        return ret, tmp

    @jit
    def check_dimension(self, available, _min, _max):
        i = []
        for c in available:
            if not (_min < c < _max):
                i.append(c)
        if len(i) > 0:
            for j in i:
                available.remove(j)
        return available

