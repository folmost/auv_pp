import numpy as np, math, rospy, time, octomap
from utils import dist, Node_rrt as Node, AUV_THICKNESS
import sys
import psutil
import multiprocessing

# Radius of closeness for rerouting a node
NEIGHBOR_RADIUS = 100
INCREMENT_DISTANCE = 30
MAX_ITERATIONS = 100000
MIN_ITERATIONS = 500


class RRTstar:
    def __init__(self, ros_node, start, goal, world_dim):
        self.goal_node = None
        self.start = start
        self.goal = goal
        self.world_dim = world_dim
        self.ros_node = ros_node

    def planning(self, save_nodes, id, display=True):
        rospy.loginfo('Computing RRT* algorithm...')
        #np.random.seed(0)
        #Inicia algoritmo.
        nodes = save_nodes
        nodes.append(Node(self.start, None))
        i = 0
        start_time = time.time()
        cpu = 0
        ram = 0
        try:
            while i < MAX_ITERATIONS and (self.goal_node is None or i < MIN_ITERATIONS):
                #print(time.time() - start_time)
                i = i + 1
                #SampleFree (Busca un punto aleatorio y libre)
                if i % 20 == 0 and self.goal_node is not None:
                    x_rand = self.goal
                else:
                    x_rand = self.random_position(self.world_dim)
                    if display:
                        self.ros_node.viz_point_rand(x_rand, color=(1,1,0), id=1, scale=20)
                # Nearest (Vertice mas cercano al punto aleatorio)
                try:
                    x_nearest = self.Nearest(nodes,x_rand)
                except ValueError:
                    continue
                # Steer (Selecciona un punto entre el vertice y el punto aleatorio)
                _, x_new = self.steer(x_nearest.pos, x_rand)
                if display:
                    self.ros_node.viz_point_new(x_new, color=(0,1,1), id=1, scale=5)
                # Busca vertices en un radio de nearest.
                try:
                    x_near = filter(lambda node: dist(node, x_new) <= NEIGHBOR_RADIUS
                                        and not self.ros_node.cast_ray(node.pos, x_new, radius=AUV_THICKNESS)[0]
                                        and node is not self.goal_node, nodes)   #Busca vecinos en un radio, revisa que no haya obstaculos.
                except KeyboardInterrupt:
                    sys.exit(1)
                if display:
                    j = 1
                    for x in x_near:
                        self.ros_node.viz_point_near(x.pos, color=(0,1,0), id=j, scale=3.5)
                        j=j+1
                # Select best possible neighbor
                try:
                    x_nearest = min(x_near, key=lambda node: node.cost + dist(node, x_new))
                except ValueError:
                    pass
                if display:
                    #Visualize point in RViz
                    self.ros_node.viz_point(x_new, color=(1,0,0), id=id + i, scale=3.5)
                x_new = Node(x_new, x_nearest)
                nodes.append(x_new)
                # Rewiring of the tree
                for node in x_near:
                    if x_new.cost + dist(x_new, node) < node.cost:
                        node.parent = x_new
                        node.cost = x_new.cost + dist(x_new, node)
                if self.goal_node is None and not self.ros_node.cast_ray(x_new.pos, self.goal, radius=AUV_THICKNESS)[0]:
                    self.goal_node = Node(self.goal, x_new)
                    nodes.append(self.goal_node)
                elif self.goal_node is not None and not self.ros_node.cast_ray(x_new.pos, self.goal, radius=AUV_THICKNESS)[0] and x_new.cost + dist(x_new.pos, self.goal) < self.goal_node.cost:
                    # Goal node rewiring
                    self.goal_node.parent = x_new
                    self.goal_node.cost = x_new.cost + dist(x_new, node)
                cpu_ = psutil.cpu_percent()/multiprocessing.cpu_count()
                ram_ = psutil.virtual_memory().percent
                if cpu_ > cpu:
                    cpu = cpu_
                if ram_ > ram:
                    ram = ram_


            if self.goal_node is None:
                raise ValueError('No Path Found')
            path = self.build_path(self.goal_node)
            return path, nodes, id + i, cpu, ram
        except KeyboardInterrupt:
            sys.exit(1)

    def project(ros_node, origin, dest, distance=INCREMENT_DISTANCE):
        v = (np.array(dest) - np.array(origin))
        vprime = v * distance / np.linalg.norm(v)
        new = np.array(origin) + vprime
        return None if ros_node.is_point_occupied(new) else [float(new[0]), float(new[1]), float(new[2])]

    def build_path(self, current):
        path = []
        actual_position = self.ros_node.get_position()
        while current:
            path.append(list(current.pos))
            current = current.parent
        path.pop(-1)
        path.append(actual_position)
        return path[::-1]

    def rand(self, a, b=None, integer=False):
        if integer:
            return np.random.randint(a) if b is None else np.random.randint(b - a) + a
        else:
            return np.random.uniform(0, a) if b is None else np.random.uniform(a, b)

    def random_position(self, world_dim):
        xmin, xmax, ymin, ymax, zmin, zmax = world_dim
        return [self.rand(xmin, xmax), self.rand(ymin, ymax), self.rand(zmin, zmax)]

    def Nearest(self, node, rand):
        nearest = min(node, key=lambda node: dist(node, rand) if node is not self.goal_node else 1e1000)
        return nearest

    def steer(self, origin, dest):
        # Obtain the new node by casting the nearest node. The new node can be against a wall
        # Envia un ray desde origin en direccion de dest, hasta una distancia maxima de max_dist.
        new = self.ros_node.cast_ray(origin, dest, radius=AUV_THICKNESS, max_dist=INCREMENT_DISTANCE)
        return new

