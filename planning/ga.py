#!/usr/bin/env python
import numpy as np
from numpy import random
from paths_generators import PathsGenerators, RADIUS
from geometry_msgs.msg import Point
from quicksort import quickSort
from common_point import CommonPoint
from genetic_algorithm import GeneticAlgorithm
from time import sleep
import psutil

debugging = False
#debugging = True

class GA:
    class Node:
        def __init__(self, pos, fitnes=0):
            self.pos = pos
            self.fitness = 0

        def __repr__(self):
            return "Nodes :pos: %s" % (self.pos)

    def __init__(self, ros_node, start, goal, world_dim):
        self.ros_node = ros_node
        self.start = start
        self.goal = goal
        self.world_dim = world_dim
        self.thickness = 5
        self.population = 12
        self.std = 5
        self.mutation_rate = 10
        self.tournament_size = 2

    def planning(self, save_nodes, id, display=True):
        #np.random.seed(10)
        # Create a 3D Occupancy Grid
        print("Start from ", self.start)
        print '\033[94m Generating random initial solutions... \033[0m'
        ga = GeneticAlgorithm(self.std, self.mutation_rate, self.goal, self.world_dim)
        self.paths = self.initialRandomSolutions()
        max_generations = 50 # We should better wait for convergence
        for p in range(len(self.paths)):
            self.paths[p].fitness = ga.fitness(self.paths[p].pos)

        max_p = max(self.paths, key=lambda x:x.fitness)
        max_f = 0
        count = 0
        cpu = 0
        ram = 0
        for i in range(max_generations):
            quickSort(self.paths)
            print '\033[94m Current maxnimum fitness:\033[0m\033[92m ' + str(self.paths[0].fitness) + '\033[0m\033[94m, Generation:\033[0m\033[92m ' + str(i) +' \033[0m'
            if max_f < self.paths[0].fitness:
                max_f = self.paths[0].fitness
                count = 0
                print("Count: ", count)
            else:
                count += 1
                if count >= 100:
                    # Looks like the algorithm converged (or won't ever converge!)
                    break
            self.paths = ga.new_generation(self.paths)
            #self.ros_node.viz_path(self.paths[0].pos,color=(1.0,0.0,0.0), scale=10) 
            cpu_ = psutil.cpu_percent()
            ram_ = psutil.virtual_memory().percent
            if cpu_ > cpu:
                cpu = cpu_
            if ram_ > ram:
                ram = ram_
            #sleep(0.5)
        quickSort(self.paths)
        self.paths[0].pos.append(tuple(self.goal))
        nodes = []
        id = 0
        return self.paths[0].pos, nodes, id, cpu, ram

    def initialRandomSolutions(self):
        generator = PathsGenerators(self.world_dim, self.ros_node, self.goal)
        solutions = []
        #print("Ruta encontrada: ", solutions)
        for j in range(self.population):
            tmp = generator.get_solutions(self.start)
            solutions.append(self.Node(tmp))
            #self.ros_node.viz_path(tmp,color=(0.0,1.0+j,0.0), scale=10) 
            print(j+1, "Rutas encontradas")
        return solutions

