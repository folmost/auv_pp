#!/usr/bin/env python
import copy
from random import randint, shuffle
from geometry_msgs.msg import Point
from numpy import random
from common_point import CommonPoint
from time import sleep
from math import sqrt
from numba import jit

class GeneticAlgorithm(object):
    class Node:
        def __init__(self, pos, fitnes=0):
            self.pos = pos
            self.fitness = 0

        def __repr__(self):
            return "Nodes :pos: %s" % (self.pos)

    def __init__(self, std, mutation_rate, goal, world_dim):
        self.std = std
        self.mutation_rate = mutation_rate
        self.goal = goal
        self.world_dim = world_dim

    def new_generation(self, paths):
        #random.seed(10)
        self.paths = paths
        new_paths = []
        print 'Tournament'
        for j in range((len(self.paths)/2)):
            new_paths.append(self.paths[j].pos)
        path = self.tournament_selection()
        point = 0
        print 'Crossover'
        while point < len(path):
            new_path = self.crossover(path[point], path[point+1])
            new_paths.append(new_path)
            new_path = self.crossover(path[point + 1], path[point])
            new_paths.append(new_path)
            point += 2
        #sleep(0.1)
        print 'Mutation'
        new_paths_ = []
        for j in range(len(new_paths)):
            np = self.mutation(new_paths[j], self.world_dim)
            path = self.Node(np)
            new_paths_.append(path)
        for p in range(len(new_paths_)):
            new_paths_[p].fitness = self.fitness(new_paths_[p].pos)
        self.paths = copy.deepcopy(new_paths_)
        #sleep(0.5)
        return self.paths

    def crossover(self, path1, path2):
        tmp = list(set(path1).intersection(path2))
        common_points = []
        for c in tmp:
            common_points.append(CommonPoint(c, path1.index(c), path2.index(c)))
        tmp = []
        common_points.sort(key=lambda x:x.path2_index)
        start_index = 0
        streak = False
        common_points_ = common_points[:]
        i = 1
        # Eliminate consecutive common points
        while i+1 < len(common_points):
            for j in range(i, len(common_points)):
                if common_points[j].path2_index - common_points[i].path2_index <= 1:
                    if j+1 < len(common_points):
                        # Keep the last point of the consecutive points
                        if common_points[j+1].path2_index - common_points[j].path2_index == 1:
                            if common_points[j].point not in self.goal:
                                common_points_.remove(common_points[j])
                    i = j
                else:
                    i += 1
                    break

        common_points_1 = common_points_[:]
        common_points_1.sort(key=lambda x:x.path1_index)

        c_tmp = common_points_[:]
        c1_tmp = common_points_1[:]

        # Eliminate points that are not in the same order (thus, not part of the same sub-path)
        for tc in range(1, len(c_tmp)):
            keep_it = False
            tc1 = c1_tmp.index(c_tmp[tc])
            if c_tmp[tc-1] == c1_tmp[tc1-1]:
                if tc + 1 < len(c_tmp) and tc1 + 1 < len(c1_tmp):
                    if c_tmp[tc+1] == c1_tmp[tc1+1] and c_tmp[tc-1] in common_points_:
                        keep_it = True
            if not keep_it:
                del common_points_[common_points_.index(c_tmp[tc])]
                del common_points_1[common_points_1.index(c1_tmp[tc1])]
        #Obtiene punto para iniciar cruce.
        crossover_point = 0
        if len(common_points_) > 1:
            crossover_point = randint(0, len(common_points_)-1)
            while common_points_[crossover_point] == common_points_1[-1]:
                crossover_point = randint(0, len(common_points_)-2)
        crossover_stop = common_points_1.index(common_points_[crossover_point])+1
        first_half = copy.deepcopy(path2[:common_points_[crossover_point].path2_index])
        new_part = []
        if crossover_stop < len(common_points_1):
            new_part = copy.deepcopy(path1[common_points_[crossover_point].path1_index:common_points_1[crossover_stop].path1_index+1])
        else:
            new_part = copy.deepcopy(path1[common_points_[crossover_point].path1_index:])
        second_half = []
        if crossover_stop < len(common_points_1):
            if not common_points_1[crossover_stop].point == path1[-1]:
                second_half = copy.deepcopy(path2[common_points_1[crossover_stop].path2_index+1:])
        new_path = first_half + new_part + second_half
        return new_path

    @jit
    def mutation(self, new_path, world_dim):
        xmin, xmax, ymin, ymax, zmin, zmax = world_dim
        for i in range(1,len(new_path)):
            if randint(0,100) > 100-self.mutation_rate and new_path[i] != self.goal:
                p = list((int(random.normal(new_path[i][0], self.std)), int(random.normal(new_path[i][1], self.std)), int(random.normal(new_path[i][2], 0.1))))
                if p[0] <= xmin:
                    p[0] = xmin + 1
                elif p[0] >= xmax:
                    p[0] = xmax - 1

                if p[1] <= ymin:
                    p[1] = ymin + 1
                elif p[1] >= ymax:
                    p[1] = ymax - 1

                if p[2] <= zmin:
                    p[2] = zmin + 1
                elif p[2] >= zmax:
                    p[2] = zmax - 1

                if p not in new_path:
                    new_path[i] = tuple(p)
        return new_path

    @jit
    def tournament_selection(self):
        bests_paths = []
        for j in range(len(self.paths)/2):
            bests_paths.append(self.paths[j].pos)
        random.shuffle(bests_paths)
        #  parents_1 = [self.new_paths[0], self.new_paths[1]]
        #  parents_2 = [self.new_paths[2], self.new_paths[3]]
        #  parents_3 = [self.new_paths[4], self.new_paths[5]]
        #  print("Parent 1: " + str(parents_1[0].fitness) + " " + str(parents_1[1].fitness))
        #  print("Parent 2: " + str(parents_2[0].fitness) + " " + str(parents_2[1].fitness))
        #  print("Parent 3: " + str(parents_3[0].fitness) + " " + str(parents_3[1].fitness))
        return bests_paths#parents_1, parents_2, parents_3

    @jit
    def fitness(self, path):
        fitness_ = 0
        for i in range(len(path)-1):
            # The closer to an obstacle, the more points lost
            x_ = (path[i+1][0]-path[i][0])**2
            y_ = (path[i+1][1]-path[i][1])**2
            z_ = (path[i+1][2]-path[i][2])**2
            fitness_ += sqrt(x_ + y_ + z_)

        fitness_ += len(path)
        return 1.0 / fitness_

    def check_path(self, path):
        cell = False
        i = 0
        while i < len(path) - 2:
            origin = path[i]
            dest = path[i+1]
            cell = self.ros_node.cast_ray(origin, dest, radius=8)[0]
            if cell:
                #self.ros_node.viz_point_rand(origin, color=(1,1,0), id=0, scale=20)
                #self.ros_node.viz_point_rand(dest, color=(0,1,0), id=1, scale=20)
                break
            i += 1
        return cell
